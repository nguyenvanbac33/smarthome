package com.da.smarthome.model;

import java.io.Serializable;

public class Room implements Serializable {
    private String icon;
    private Long id;
    private String name;
    private float humidity;
    private float temp;
    private float gas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getTemp() {
        return temp;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getGas() {
        return gas;
    }

    public void setGas(float gas) {
        this.gas = gas;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
