package com.da.smarthome.adapter;

import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.da.smarthome.R;
import com.da.smarthome.model.Device;

import java.util.ArrayList;

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.StudentHolder> {

    private LayoutInflater inflater;
    private ArrayList<Device> data;
    private StudentItemListener listener;

    public DeviceAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setListener(StudentItemListener listener) {
        this.listener = listener;
    }

    public void setData(ArrayList<Device> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public ArrayList<Device> getData() {
        return data;
    }

    @NonNull
    @Override
    public StudentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StudentHolder(inflater.inflate(R.layout.item_device, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StudentHolder holder, int position) {
        holder.bindData(data.get(position));
        if (listener != null) {
            holder.itemView.setOnLongClickListener(view -> {
                listener.onDeviceLongClicked(data.get(position));
                return true;
            });
            holder.itemView.setOnClickListener(view -> {
                listener.onDeviceClicked(data.get(position));
            });
            holder.sbSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if (b) {
                        listener.onDeviceChangeSpeed(data.get(position), seekBar.getProgress());
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class StudentHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvSpeed;
        private SeekBar sbSpeed;
        private View container;

        public StudentHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvSpeed = itemView.findViewById(R.id.tv_speed);
            sbSpeed = itemView.findViewById(R.id.sb_speed);
            container = itemView.findViewById(R.id.container);
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        private void bindData(Device device) {
            tvName.setText(device.getName());
            tvSpeed.setText("Speed: " + device.getValue());
            sbSpeed.setMin(device.getMinValue());
            sbSpeed.setMax(device.getMaxValue());
            sbSpeed.setProgress(device.getValue());
            sbSpeed.setEnabled(device.isOn());
            if (device.isOn()) {
                container.setBackgroundColor(Color.WHITE);
            } else {
                container.setBackgroundColor(Color.LTGRAY);
            }
        }
    }

    public interface StudentItemListener {
        void onDeviceClicked(Device device);

        void onDeviceChangeSpeed(Device device, int value);

        void onDeviceLongClicked(Device device);
    }
}
