package com.da.smarthome.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.da.smarthome.fragments.RoomFragment;
import com.da.smarthome.model.Room;

import java.util.ArrayList;

public class RoomAdapter extends FragmentPagerAdapter {
    private ArrayList<Room> data;
    public RoomAdapter(FragmentManager fm) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    public void setData(ArrayList<Room> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return RoomFragment.getInstance(data.get(position));
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return data.get(position).getName();
    }
}
