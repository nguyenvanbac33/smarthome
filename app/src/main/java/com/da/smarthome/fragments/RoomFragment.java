package com.da.smarthome.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.da.smarthome.R;
import com.da.smarthome.activity.AddDeviceActivity;
import com.da.smarthome.adapter.DeviceAdapter;
import com.da.smarthome.model.Device;
import com.da.smarthome.model.Room;
import com.da.smarthome.utils.Constance;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RoomFragment extends Fragment implements DeviceAdapter.StudentItemListener, View.OnClickListener, ValueEventListener {
    private Room room;

    private RecyclerView lvDevice;
    private DeviceAdapter adapter;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private FloatingActionButton btnAdd;

    private TextView tvName;
    private TextView tvTemp;
    private TextView tvHumidity;
    private TextView tvGas;
    private ImageView imIcon;

    public static RoomFragment getInstance(Room room) {
        RoomFragment instance = new RoomFragment();
        instance.room = room;
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_room, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        database.getReference(Constance.ROOM).child(room.getId().toString()).child(Constance.DEVICES).addValueEventListener(this);
        initViews();
        bindData(room);
    }

    private void initViews() {
        lvDevice = getView().findViewById(R.id.lv_device);
        adapter = new DeviceAdapter(getLayoutInflater());
        adapter.setListener(this);
        lvDevice.setAdapter(adapter);
        btnAdd = getView().findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);

        imIcon = getView().findViewById(R.id.ic_room);
        tvName = getView().findViewById(R.id.tv_room_name);
        tvTemp = getView().findViewById(R.id.tv_room_temp);
        tvHumidity = getView().findViewById(R.id.tv_room_humidity);
        tvGas = getView().findViewById(R.id.tv_room_gas);
    }

    private void bindData(Room room) {
        tvName.setText(room.getName());
        if (room.getTemp() > 0) {
            tvTemp.setText(room.getTemp() + "°C");
            tvTemp.setVisibility(View.VISIBLE);
        } else {
            tvTemp.setVisibility(View.GONE);
        }
        if (room.getHumidity() > 0) {
            tvHumidity.setVisibility(View.VISIBLE);
            tvHumidity.setText(room.getHumidity() + "%");
        } else {
            tvHumidity.setVisibility(View.GONE);
        }
        if (room.getGas() > 0) {
            tvGas.setVisibility(View.VISIBLE);
            if (room.getGas() < 1000) {
                tvGas.setText("Tốt");
            } else if (room.getGas() < 1500) {
                tvGas.setText("Bình thường");
            } else {
                tvGas.setText("KHẨN CẤP, dễ hoả hoạn");
            }
        } else {
            tvGas.setVisibility(View.GONE);
        }
        Glide.with(imIcon).load(room.getIcon()).into(imIcon);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot snapshot) {
        ArrayList<Device> arr = new ArrayList<>();
        for (DataSnapshot sn : snapshot.getChildren()) {
            Device st = sn.getValue(Device.class);
            arr.add(st);
        }
        adapter.setData(arr);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError error) {

    }

    @Override
    public void onDeviceChangeSpeed(Device device, int value) {
        device.setValue(value);
        database.getReference(Constance.ROOM).child(room.getId().toString()).child(Constance.DEVICES).child(device.getId() + "").setValue(device);
    }

    @Override
    public void onDeviceClicked(Device device) {
        device.setOn(!device.isOn());
        database.getReference(Constance.ROOM).child(room.getId().toString()).child(Constance.DEVICES).child(device.getId() + "").setValue(device);
    }

    @Override
    public void onDeviceLongClicked(Device device) {
        new AlertDialog.Builder(getContext())
                .setTitle(device.getName())
                .setMessage("Bạn muốn xoá thiết bị này không?")
                .setNegativeButton("Huỷ", (dialogInterface, i) -> dialogInterface.dismiss())
                .setPositiveButton("Đồng ý", (dialogInterface, i) -> {
                    database.getReference(Constance.ROOM).child(room.getId().toString()).child(Constance.DEVICES).child(device.getId() + "").removeValue();
                    dialogInterface.dismiss();
                })
                .create()
                .show();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getContext(), AddDeviceActivity.class);
        intent.putExtra(Room.class.getName(), room);
        startActivity(intent);
    }
}
