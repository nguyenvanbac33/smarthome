package com.da.smarthome.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedUtils {
    private Context context;
    private SharedPreferences sharedPreferences;

    public SharedUtils(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("SharedUtils", Context.MODE_PRIVATE);
    }

    public void set(String key, String value) {
        sharedPreferences.edit().putString(key, value).commit();
    }

    public String get(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void logout() {
        sharedPreferences.edit().clear().commit();
    }
}
