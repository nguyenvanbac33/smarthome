package com.da.smarthome.utils;

public interface Constance {
    String ACCOUNT = "account";
    String ROOM = "room";
    String DEVICES = "devices";
    String KEY_USER_NAME = "KEY_USER_NAME";
    String ELECTRONIC = "electronic";
}
