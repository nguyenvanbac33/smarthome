package com.da.smarthome.utils;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogUtils {
    private static ProgressDialog dialog;

    public static void showProgressDialog(Context context) {
        dismissProgressDialog();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void dismissProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
