package com.da.smarthome.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.da.smarthome.R;
import com.da.smarthome.model.Device;
import com.da.smarthome.model.Room;
import com.da.smarthome.utils.Constance;
import com.da.smarthome.views.ActionbarView;
import com.google.firebase.database.FirebaseDatabase;

public class AddDeviceActivity extends AppCompatActivity implements View.OnClickListener {
    private Room room;
    private EditText edtName;
    private EditText edtMaxSpeed;
    private Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);
        ((ActionbarView) findViewById(R.id.actionbar)).setTitle("Thêm thiết bị");
        ((ActionbarView) findViewById(R.id.actionbar)).showBack();
        room = (Room) getIntent().getSerializableExtra(Room.class.getName());
        init();
    }

    private void init() {
        edtName = findViewById(R.id.edt_name);
        edtMaxSpeed = findViewById(R.id.edt_max_speed);
        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String name = edtName.getText().toString();
        String maxSpeed = edtMaxSpeed.getText().toString();
        if (name.isEmpty() || maxSpeed.isEmpty()/* || power.isEmpty() || performance.isEmpty() || voltage.isEmpty() || electric.isEmpty()*/) {
            Toast.makeText(this, "Chưa nhập đủ thông tin", Toast.LENGTH_SHORT).show();
            return;
        }
        Device device = new Device();
        device.setName(name);
        device.setId(System.currentTimeMillis());
        device.setValue(0);
        device.setOn(false);
        device.setMaxValue(Integer.parseInt(maxSpeed));
        device.setMinValue(0);
        FirebaseDatabase.getInstance().getReference(Constance.ROOM).child(room.getId() + "")
                .child(Constance.DEVICES).child(device.getId() + "")
                .setValue(device);
        finish();
    }
}
