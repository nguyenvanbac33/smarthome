package com.da.smarthome.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.da.smarthome.R;
import com.da.smarthome.adapter.RoomAdapter;
import com.da.smarthome.model.Electronic;
import com.da.smarthome.model.Room;
import com.da.smarthome.utils.Constance;
import com.da.smarthome.utils.DialogUtils;
import com.da.smarthome.utils.SharedUtils;
import com.da.smarthome.views.ActionbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RoomActivity extends AppCompatActivity implements ValueEventListener {
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constance.ROOM);
    private ViewPager pager;
    private RoomAdapter adapter;
    private TextView tvPerformance;
    private TextView tvElectric;
    private TextView tvVoltage;
    private TextView tvPower;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        DialogUtils.showProgressDialog(this);
        reference.addValueEventListener(this);
        FirebaseDatabase.getInstance().getReference(Constance.ELECTRONIC).addValueEventListener(this);
        initViews();
    }

    private void initViews() {
        tvPerformance = findViewById(R.id.tv_performance);
        tvElectric = findViewById(R.id.tv_electric);
        tvVoltage = findViewById(R.id.tv_voltage);
        tvPower = findViewById(R.id.tv_power);

        pager = findViewById(R.id.pager);
        adapter = new RoomAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        ((ActionbarView) findViewById(R.id.actionbar)).setTitle("");
        ((ActionbarView) findViewById(R.id.actionbar)).setExit(view -> {
            new SharedUtils(this).logout();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        });
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot snapshot) {
        if (snapshot.getKey().equals(Constance.ELECTRONIC)) {
            Electronic electronic = snapshot.getValue(Electronic.class);
            tvPerformance.setText(electronic.getPerformance() + "W");
            tvPower.setText(electronic.getPower() + "Wh");
            tvVoltage.setText(electronic.getVoltage() + "V");
            tvElectric.setText(electronic.getElectric() + "A");
            return;
        }
        ArrayList<Room> arr = new ArrayList<>();
        for (DataSnapshot sn : snapshot.getChildren()) {
            Room st = sn.getValue(Room.class);
            arr.add(st);
        }
        adapter.setData(arr);
        DialogUtils.dismissProgressDialog();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError error) {

    }
}
