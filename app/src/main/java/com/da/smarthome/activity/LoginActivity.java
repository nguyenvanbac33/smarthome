package com.da.smarthome.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.da.smarthome.R;
import com.da.smarthome.model.Account;
import com.da.smarthome.utils.Constance;
import com.da.smarthome.utils.DialogUtils;
import com.da.smarthome.utils.SharedUtils;
import com.da.smarthome.views.ActionbarView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private SharedUtils sharedUtils;
    private EditText edtUserName;
    private EditText edtPassword;
    private Button btnLogin;
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(Constance.ACCOUNT);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedUtils = new SharedUtils(this);
        login();
        initViews();
    }

    private void initViews() {
        edtPassword = findViewById(R.id.edt_password);
        edtUserName = findViewById(R.id.edt_user_name);
        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String userName = edtUserName.getText().toString();
        String password = edtPassword.getText().toString();
        if (userName.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "User name and password cannot blank", Toast.LENGTH_SHORT).show();
            return;
        }
        DialogUtils.showProgressDialog(this);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                DialogUtils.dismissProgressDialog();
                try {
                    Account t = snapshot.getValue(Account.class);
                    if (t.getPassword().equalsIgnoreCase(password) && t.getUserName().equalsIgnoreCase(userName)) {
                        sharedUtils.set(Constance.KEY_USER_NAME, t.getUserName());
                        login();
                        return;
                    }
                    Toast.makeText(LoginActivity.this, "Login fail", Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Login fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void login() {
        if (!sharedUtils.get(Constance.KEY_USER_NAME).isEmpty()) {
            Intent intent = new Intent(LoginActivity.this, RoomActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
