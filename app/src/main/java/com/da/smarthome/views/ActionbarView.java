package com.da.smarthome.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.da.smarthome.R;

public class ActionbarView extends FrameLayout implements View.OnClickListener {

    private TextView tvTitle;
    private ImageView imExitApp;
    private ImageView imBack;
    private View line;

    public ActionbarView(@NonNull Context context) {
        super(context);
        init();
    }

    public ActionbarView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ActionbarView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ActionbarView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.actionbar_view, this, true);
        tvTitle = findViewById(R.id.tv_title);
        imExitApp = findViewById(R.id.im_exit);
        imBack = findViewById(R.id.im_back);
        line = findViewById(R.id.line);
        imBack.setOnClickListener(this);
    }

    public void setExit(OnClickListener listener) {
        imExitApp.setVisibility(VISIBLE);
        imExitApp.setOnClickListener(listener);
    }

    public void setTitle(String title) {
        tvTitle.setText(title);
        if (title.isEmpty()) {
            line.setVisibility(GONE);
        }
    }

    public void showBack() {
        imBack.setVisibility(VISIBLE);
    }

    @Override
    public void onClick(View view) {
        ((AppCompatActivity) getContext()).finish();
    }
}
